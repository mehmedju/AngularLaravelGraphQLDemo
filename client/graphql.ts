import gql from 'graphql-tag';

export const GetHeadline = gql`
  query headline {
    headline {
      value
    }
  }
`;

export const UpdateHeadline = gql`
  mutation headline($value: String!) {
    updateHeadline(value: $value) {
      value
    }
  }
`;
