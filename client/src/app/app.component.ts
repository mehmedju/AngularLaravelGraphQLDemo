import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import * as graphql from '../../graphql';
import { headlineQuery } from '../../querySchema';
import { headlineMutation } from '../../mutationsSchema';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})

export class AppComponent implements OnInit {
  constructor(private apollo: Apollo) {
  }
  title: string = 'app';
  headline: string = '';
  isEditMode: boolean = false;
  currentHeadline: string;

  ngOnInit() {
    this.apollo.watchQuery<headlineQuery>({
      query: graphql.GetHeadline
    }).subscribe(({data}) => {
      this.currentHeadline = data.headline[0].value;
    }, (error) => {
      console.log('there was an error sending the query', error);
    });
  }

  submitHeadline(updatedHeadline: string): void {
    this.apollo.mutate({
      mutation: graphql.UpdateHeadline,
      variables: {
        value: updatedHeadline
      }
    }).subscribe((response) => {
      const data: headlineMutation = <headlineMutation>response.data;
      this.currentHeadline = data.updateHeadline.value;
    }, (error) => {
      console.log('there was an error sending the query', error);
    });
    this.isEditMode = false;
  }

  setEditMode(event): void {
    event.preventDefault();
    this.headline = this.currentHeadline;
    this.isEditMode = true;
  }

}
