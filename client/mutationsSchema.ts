/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type headlineMutationVariables = {
  value: string,
};

export type headlineMutation = {
  updateHeadline:  {
    // The value of headline
    value: string | null,
  } | null,
};
/* tslint:enable */
