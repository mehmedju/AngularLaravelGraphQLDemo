<?php
namespace App\GraphQL\Mutation;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\User;
use App\Headline;

class HeadlineMutation extends Mutation {

	protected $attributes = [
		'name' => 'updateHeadline'
	];

	public function type()
	{
		return GraphQL::type('Headline');
	}

	public function args()
	{
		return [
			'value' => ['name' => 'value', 'type' => Type::nonNull(Type::string())]
		];
	}

	public function resolve($root, $args)
	{
                $headline = Headline::where('id', 1)->first();
                
                if($headline){
                    $headline->value = $args['value'];
                    $headline->save();
                } else {
                    $headline = new Headline();
                    $headline->value = $args['value'];
                    $headline->save();
                }
		return $headline;
	}

}
