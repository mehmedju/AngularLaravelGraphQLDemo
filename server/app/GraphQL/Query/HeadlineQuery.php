<?php
namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Headline;

class HeadlineQuery extends Query {

	protected $attributes = [
		'name' => 'Headline'
	];

	public function type()
	{
		return Type::listOf(GraphQL::type('Headline'));
	}

	public function args()
	{
		return [
			'name' => ['name' => 'value', 'type' => Type::string()]
		];
	}

	public function resolve()
	{
            $headlines = Headline::all();
            return $headlines;
  }
}
