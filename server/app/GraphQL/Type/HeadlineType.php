<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class HeadlineType extends GraphQLType {

	protected $attributes = [
		'name' => 'Headline',
		'description' => 'A value'
	];

	public function fields()
	{
		return [
			'value' => [
				'type' => Type::string(),
				'description' => 'The value of headline'
			]
		];
	}
}
