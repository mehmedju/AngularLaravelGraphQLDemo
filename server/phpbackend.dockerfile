FROM php:7
RUN apt-get update -y && apt-get install -y openssl zip unzip git libpq-dev && docker-php-ext-install pdo pdo_pgsql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring

WORKDIR /app

COPY . /app

RUN composer install
RUN composer update
RUN cp .env.example .env
RUN php artisan key:gen

EXPOSE 8080